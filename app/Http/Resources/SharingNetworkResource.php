<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SharingNetworkResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->getKey(),
            'url'                 => route('tenant.home', $this->subdomain),
            'name'                => $this->name,
            'subdomain'           => $this->subdomain,
            'privacy_level'       => $this->privacy_level,
            'privacy_level_label' => config('anyshare.sharing-networks.privacy-levels.' . $this->privacy_level),
            'tenant'              => $this->tenant,
            'public'              => $this->privacy_level === 'O',
            'private'             => $this->privacy_level === 'C',
        ];
    }
}
