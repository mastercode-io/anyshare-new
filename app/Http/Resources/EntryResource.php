<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Str;

class EntryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->getKey(),
            'url'                  => '#',
            'title'                => $this->title,
            'quantity'             => $this->quantity,
            'latitude'             => $this->latitude,
            'longitude'            => $this->longitude,
            'type'                 => $this->type,
            'type_label'           => Str::upper($this->type),
            'enabled'              => $this->enabled,
            'visible'              => $this->visible,
            'tags'                 => !empty($this->tags) ? implode(', ', $this->tags) : '',
            'expires_at'           => $this->expires_at->format('J Y'),
            'created_at'           => $this->created_at->format('J Y'),
            'exchange_types_label' => $this->getExchangeTypesLabel(),
        ];
    }

    /**
     * @return string
     */
    protected function getExchangeTypesLabel(): string
    {
        if ($this->exchangeTypes()->count() === 0) {
            return '';
        }

        return $this->exchangeTypes->pluck('name')->implode(', ');
    }
}
