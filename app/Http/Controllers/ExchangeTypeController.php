<?php

namespace App\Http\Controllers;

use App\Entities\ExchangeType;
use Illuminate\Http\Request;

class ExchangeTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\ExchangeType  $exchangeType
     * @return \Illuminate\Http\Response
     */
    public function show(ExchangeType $exchangeType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entities\ExchangeType  $exchangeType
     * @return \Illuminate\Http\Response
     */
    public function edit(ExchangeType $exchangeType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\ExchangeType  $exchangeType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExchangeType $exchangeType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\ExchangeType  $exchangeType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExchangeType $exchangeType)
    {
        //
    }
}
