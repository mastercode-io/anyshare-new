<?php

namespace App\Http\Controllers\SharingNetworks;

use App\Entities\SharingNetwork;
use App\Http\Controllers\Controller;

class EditSharingNetworkHandler extends Controller
{
    public function __invoke(SharingNetwork $sharingNetwork)
    {
        $privacyLevels = collect(['' => ''])->merge(config('anyshare.sharing-networks.privacy-levels'));

        return view('share.edit')->with([
            'privacy_levels' => $privacyLevels,
            'sharingNetwork' => $sharingNetwork,
        ]);
    }
}
