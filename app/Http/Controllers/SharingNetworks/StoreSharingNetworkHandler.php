<?php

namespace App\Http\Controllers\SharingNetworks;

use App\Entities\SharingNetwork;
use App\Http\Controllers\Controller;
use App\Repositories\SharingNetworkRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StoreSharingNetworkHandler extends Controller
{
    public function __invoke(Request $request)
    {
        $input = $request->validate([
            'name'          => 'required|max:255',
            'subdomain'     => 'required|max:30|unique:sharing_networks',
            'privacy_level' => [
                'required',
                Rule::in(array_keys(config('anyshare.sharing-networks.privacy-levels'))),
            ],
        ]);

        $item = SharingNetwork::create($input);

        return redirect($item->url);
    }
}
