<?php

namespace App\Http\Controllers\SharingNetworks;

use App\Http\Controllers\Controller;

class CreateSharingNetworkHandler extends Controller
{
    public function __invoke()
    {
        $privacyLevels = collect(['' => ''])->merge(config('anyshare.sharing-networks.privacy-levels'));

        return view('share.create')->with([
            'privacy_levels' => $privacyLevels,
        ]);
    }
}
