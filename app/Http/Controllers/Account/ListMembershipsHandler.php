<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Resources\SharingNetworkResource;
use Illuminate\Http\Request;

class ListMembershipsHandler extends Controller
{
    public function __invoke(Request $request)
    {
        $sharingNetworks = SharingNetworkResource::collection($request->user()->sharingNetworks);

        return view('account.memberships')->with([
            'sharing_networks' => $sharingNetworks,
        ]);
    }
}
