<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 04/01/18
 * Time: 23:44
 */

namespace App\Http\Controllers\Tenant;

use App\Entities\SharingNetwork;
use App\Http\Controllers\Controller;
use App\Http\Resources\EntryResource;
use App\Repositories\EntryRepository;

class HomeHandler extends Controller
{
    public function __invoke(SharingNetwork $sharingNetwork)
    {
        return view('tenant.home')->with([
            'entries' => EntryResource::collection($sharingNetwork->entries),
        ]);
    }

}