<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\TenantNotFoundException;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->tenant->privacy_level === 'O') {
            return $next($request);
        }

        if (!Auth::check() || Auth::user()->cannot('view', $request->tenant)) {
            abort(403);
        }

        $request->tenant->connect();
        return $next($request);
    }
}
