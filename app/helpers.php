<?php

/**
 * HELPERS
 */

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

if (!function_exists('theme')) {
    /**
     * Determine the path of asset based on the current theme.
     *
     * @param      $path
     * @param null $secure
     * @return string
     */
    function theme($path, $secure = null)
    {
        return app('url')->asset('modules/' . config('app.theme') . '/' . $path, $secure);
    }
}

if (!function_exists('tenant_connect')) {
    /**
     * Establish a tenant database connection.
     *
     * @param $subdomain
     * @param $hostname
     * @param $username
     * @param $password
     * @param $database
     */
    function tenant_connect($subdomain, $hostname, $username, $password, $database)
    {
        // Erase the tenant connection, thus making Laravel get the default values all over again.
        DB::purge('tenant');

        // Make sure to use the database name we want to establish a connection.
        Config::set('database.connections.tenant.subdomain', $subdomain);
        Config::set('database.connections.tenant.host', $hostname);
        Config::set('database.connections.tenant.database', $database);
        Config::set('database.connections.tenant.username', $username);
        Config::set('database.connections.tenant.password', $password);
        Config::set('database.connections.tenant.options.database', $database);

        // Rearrange the connection data.
        DB::reconnect('tenant');

        // Ping the database. This will throw an exception in case the database does not exists.
        Schema::connection('tenant')->getConnection()->reconnect();
    }
}

if (!function_exists('tenant_migrate')) {
    /**
     * Run Tenant Migrations in the connected tenant database.
     */
    function tenant_migrate()
    {
        Artisan::call('migrate', [
            '--database' => 'tenant',
            '--path'     => 'database/migrations/tenant',
        ]);
    }
}

if (!function_exists('tenant_route')) {
    /**
     * Get a url to a tenant route.
     *
     * @param string $route
     * @param array  $data
     * @return string
     */
    function tenant_route(string $route, array $data = [])
    {
        return route($route, array_merge([request()->tenant->subdomain], $data));
    }
}