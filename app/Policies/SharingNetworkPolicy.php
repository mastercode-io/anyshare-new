<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Entities\SharingNetwork;
use App\Entities\User;

class SharingNetworkPolicy
{
    use HandlesAuthorization;

    public function view(User $user, SharingNetwork $sharingNetwork)
    {
        return $user->sharingNetworks()->where('_id', $sharingNetwork->getKey())->count() > 0;
    }
}
