<?php

namespace App\Entities;

use App\Entities\Observers\UserObserver;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Jenssegers\Mongodb\Relations\EmbedsOne;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected static function boot()
    {
        self::observe(UserObserver::class);
        parent::boot();
    }


    /**
     * User's profile.
     *
     * @return EmbedsOne
     */
    public function profile(): EmbedsOne
    {
        return $this->embedsOne(Profile::class);
    }

    /**
     * @return BelongsToMany
     */
    public function sharingNetworks(): BelongsToMany
    {
        return $this->belongsToMany(SharingNetwork::class)
            ->as('subscription')
            ->withTimestamps()
            ->withPivot(['role']);
    }
}
