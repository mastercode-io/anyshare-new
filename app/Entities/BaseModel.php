<?php

namespace App\Entities;

use App\Entities\Traits\HasHashid;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class BaseModel extends MongoModel
{
    use SoftDeletes, HasHashid;

    protected $connection = 'main';
}
