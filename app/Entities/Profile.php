<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'display_name',
        'lat',
        'lng',
    ];
}
