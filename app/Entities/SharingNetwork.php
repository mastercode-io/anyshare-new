<?php

namespace App\Entities;

use App\Entities\Observers\SharingNetworkObserver;
use App\Exceptions\TenantNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Config;

/**
 * @property mixed tenant
 * @property mixed entries
 */
class SharingNetwork extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'subdomain',
        'privacy_level',
        'tenant',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'tenant',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'tenant' => 'collection',
    ];

    /**
     * User roles.
     */
    const ROLES = [
        'owner'   => 1,
        'manager' => 2,
        'user'    => 3,
    ];

    /**
     * Boot sharing network.
     */
    protected static function boot()
    {
        self::observe(SharingNetworkObserver::class);

        parent::boot();
    }

    /**
     * Connect the application to this sharing network.
     */
    public function connect()
    {
        if (!$this->connected()) {
            tenant_connect(
                $this->attributes['subdomain'],
                $this->tenant->get('hostname'),
                $this->tenant->get('username'),
                $this->tenant->get('password'),
                $this->tenant->get('database')
            );
        }
    }

    /**
     * Determine if this sharing network is the the current tenant.
     *
     * @return bool
     */
    private function connected(): bool
    {
        return $this->attributes['subdomain'] === Config::get('database.connections.tenant.subdomain');
    }

    /**
     * Users registered in this sharing network.
     *
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)
            ->as('member')
            ->withTimestamps()
            ->withPivot(['role']);
    }

    /**
     * List of exchange types enabled for this sharing network.
     *
     * @return BelongsToMany
     */
    public function exchangeTypes(): BelongsToMany
    {
        return $this->belongsToMany(ExchangeType::class);
    }

    /**
     * Entries available to this sharing network.
     *
     * @return BelongsToMany
     */
    public function entries(): BelongsToMany
    {
        return $this->belongsToMany(Entry::class);
    }

    /**
     * Get the label of the entity's privacy level.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getPrivacyLevelLabelAttribute()
    {
        return config('anyshare.sharing-networks.privacy-levels.' . $this->attributes['privacy_level']);
    }

    /**
     * Get the url to access the sharing network.
     *
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('tenant.home', $this->attributes['subdomain']);
    }

    /**
     * Find a sharing network by its subdomain.
     *
     * @param string $subdomain
     * @return SharingNetwork
     */
    static public function findTenant(string $subdomain): SharingNetwork
    {
        $item = self::where('subdomain', $subdomain)->first();

        if (is_null($item)) {
            throw new TenantNotFoundException();
        }

        return $item;
    }

    /**
     * Determine if this sharing network is public.
     *
     * @return bool
     */
    public function isPublic()
    {
        return $this->attributes['privacy_level'] === 'O';
    }

    /**
     * Determine if this sharing network is private.
     *
     * @return bool
     */
    public function isPrivate()
    {
        return $this->attributes['privacy_level'] === 'C';
    }
}
