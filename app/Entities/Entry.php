<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Str;

class Entry extends BaseModel
{
    protected $fillable = [
        'title',
        'quantity',
        'latitude',
        'longitude',
        'type',
        'enabled',
        'visible',
        'tags',
        'expires_at',
        'user_id',
    ];

    protected $casts = [
        'latitude'   => 'float',
        'longitude'  => 'float',
        'quantity'   => 'integer',
        'tags'       => 'array',
        'expires_at' => 'date',
        'visible'    => 'boolean',
        'enabled'    => 'boolean',
    ];

    const TYPES = [
        'want',
        'have',
    ];

    /**
     * User who added this entry.
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Exchange types of this entry.
     *
     * @return BelongsToMany
     */
    public function exchangeTypes(): BelongsToMany
    {
        return $this->belongsToMany(ExchangeType::class);
    }

    /**
     * List of sharing networks which this entry has been added.
     *
     * @return BelongsToMany
     */
    public function sharingNetworks(): BelongsToMany
    {
        return $this->belongsToMany(SharingNetwork::class);
    }

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return '#';
    }

    /**
     * @return mixed
     */
    public function getTypeLabelAttribute()
    {
        return Str::upper($this->type);
    }
}
