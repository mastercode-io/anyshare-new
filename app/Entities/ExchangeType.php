<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ExchangeType extends BaseModel
{
    protected $fillable = [
        'name',
    ];

    /**
     * Entries which have this exchange type.
     *
     * @return BelongsToMany
     */
    public function entries(): BelongsToMany
    {
        return $this->belongsToMany(Entry::class);
    }

    /**
     * Sharing Networks in which this exchange type is enabled.
     *
     * @return BelongsToMany
     */
    public function sharingNetworks(): BelongsToMany
    {
        return $this->belongsToMany(SharingNetwork::class);
    }
}
