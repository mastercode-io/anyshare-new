<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 16/02/18
 * Time: 22:31
 */

namespace App\Entities\Traits;


use App\Entities\Observers\BaseObserver;

trait HasHashid
{
    static public function bootHasHashid()
    {
        self::observe(BaseObserver::class);
    }
}