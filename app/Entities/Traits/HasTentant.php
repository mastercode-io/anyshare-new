<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 04/01/18
 * Time: 23:25
 */

namespace App\Entities\Traits;


trait HasTentant
{
    protected $connection = 'tenant';
}