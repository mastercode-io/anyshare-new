<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 16/02/18
 * Time: 22:28
 */

namespace App\Entities\Observers;


use Hashids\Hashids;

class BaseObserver
{
    /**
     * Listen to the Model created event.
     *
     * @param $model
     */
    public function created($model)
    {
        // Add a Hash ID to the model, based on the
        // model class name.
        $hashids = new Hashids(get_class($model), 6);
        $model->hashid = $hashids->encodeHex($model->getKey());
        $model->save();
    }
}