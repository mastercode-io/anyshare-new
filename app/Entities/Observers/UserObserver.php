<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 16/02/18
 * Time: 23:56
 */

namespace App\Entities\Observers;


use App\Entities\User;

class UserObserver
{
    public function created(User $user)
    {
        // FIXME: We have to add the user's geolocation.
        // We have two main approaches here:
        //
        // 1) Get it through backend code;
        // 2) Ask the user his location, what it's the right way of doing it.

        $user->profile()->create([
            'name'         => request()->get('name'),
            'display_name' => '',
            'latitude'     => '',
            'longitude'    => '',
        ]);
    }
}