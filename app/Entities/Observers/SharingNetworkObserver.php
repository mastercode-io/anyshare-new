<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 16/02/18
 * Time: 23:50
 */

namespace App\Entities\Observers;


use App\Entities\SharingNetwork;
use Illuminate\Support\Str;

class SharingNetworkObserver
{
    public function creating(SharingNetwork $sharingNetwork)
    {
        // TODO: Add logic to generate a password and a username for the database access.
        $sharingNetwork->fill([
            'tenant' => [
                'hostname' => config('database.connections.tenant.base_host'),
                'database' => $sharingNetwork->getAttribute('subdomain') . '_' . Str::random(8),
                'password' => '',
                'username' => '',
            ],
        ]);
    }

    public function created(SharingNetwork $sharingNetwork)
    {
        $userId = auth()->user()->getKey();

        $sharingNetwork->users()->attach($userId, [
            'role' => $sharingNetwork::ROLES['owner'],
        ]);
    }
}