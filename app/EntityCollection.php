<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 28/12/17
 * Time: 00:53
 */

namespace App;


use Illuminate\Support\Collection;

class EntityCollection extends Collection
{
    /**
     * Return the entity's id.
     *
     * @return mixed
     */
    public function id()
    {
        return $this->get('_id');
    }
}