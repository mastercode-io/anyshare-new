<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 05/01/18
 * Time: 00:42
 */

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class TenantNotFoundException extends ModelNotFoundException
{
    protected $message = 'The Sharing Network could not be found';

    public function render()
    {
        return view('errors.tenant-not-found');
    }
}