<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 13/11/17
 * Time: 14:16
 */

namespace App\Exceptions;

use Exception;

class ModelUpdateException extends Exception
{
    protected $message = 'Erro ao atualizar um registro.';
}