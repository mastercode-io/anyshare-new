<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  Exception $exception
     * @return void
     */
    public function report(Exception $e)
    {
        // if (app()->bound('sentry') && $this->shouldReport($e)) {
        //     app('sentry')->captureException($e);
        // }

        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Exception                 $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        /*
         * Check if the identified error is related to CSRF-TOKEN,
         * so we redirect the user to back to the last page.
         */
        if ($e instanceof TokenMismatchException) {
            return redirect()->back()->withInput($request->all());
        }

        return parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request                 $request
     * @param  \Illuminate\Auth\AuthenticationException $e
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $e)
    {
        /*
         * Return a 401 Not Authorized JSON response.
         */
        if ($request->expectsJson()) {
            return response()->json(['message' => 'Not authorized.'], 401);
        }

        /*
         * If user is trying to login in a System route
         * we redirect him to the System login page.
         */
        if ($request->is('sys*')) {
            return redirect()->route('sys.auth.login');
        }

        /*
         * Just redirect user to the site login.
         */
        return redirect()->guest(route('auth.login'));
    }
}
