<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Make::class, function (Faker $faker) {
    $name = \Illuminate\Support\Str::studly($faker->words(1));

    return [
        'name' => $name,
        'slug' => \Illuminate\Support\Str::slug($name),
    ];
});
