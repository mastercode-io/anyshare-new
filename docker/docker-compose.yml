version: '2'

services:

### Applications Code Container #############################

    applications:
      image: tianon/true
      volumes:
        - ${APPLICATION}:/var/www

### Workspace Utilities Container ###########################

    workspace:
      build:
        context: ./workspace
        args:
          - INSTALL_XDEBUG=${WORKSPACE_INSTALL_XDEBUG}
          - INSTALL_BLACKFIRE=${INSTALL_BLACKFIRE}
          - INSTALL_SOAP=${WORKSPACE_INSTALL_SOAP}
          - INSTALL_MONGO=${WORKSPACE_INSTALL_MONGO}
          - INSTALL_PHPREDIS=${WORKSPACE_INSTALL_PHPREDIS}
          - INSTALL_MSSQL=${WORKSPACE_INSTALL_MSSQL}
          - INSTALL_NODE=${WORKSPACE_INSTALL_NODE}
          - INSTALL_YARN=${WORKSPACE_INSTALL_YARN}
          - INSTALL_DRUSH=${WORKSPACE_INSTALL_DRUSH}
          - INSTALL_DRUPAL_CONSOLE=${WORKSPACE_INSTALL_DRUPAL_CONSOLE}
          - INSTALL_AEROSPIKE=${WORKSPACE_INSTALL_AEROSPIKE}
          - INSTALL_V8JS=${WORKSPACE_INSTALL_V8JS}
          - COMPOSER_GLOBAL_INSTALL=${WORKSPACE_COMPOSER_GLOBAL_INSTALL}
          - INSTALL_WORKSPACE_SSH=${WORKSPACE_INSTALL_WORKSPACE_SSH}
          - INSTALL_LARAVEL_ENVOY=${WORKSPACE_INSTALL_LARAVEL_ENVOY}
          - INSTALL_LARAVEL_INSTALLER=${WORKSPACE_INSTALL_LARAVEL_INSTALLER}
          - INSTALL_DEPLOYER=${WORKSPACE_INSTALL_DEPLOYER}
          - INSTALL_LINUXBREW=${WORKSPACE_INSTALL_LINUXBREW}
          - INSTALL_MC=${WORKSPACE_INSTALL_MC}
          - INSTALL_SYMFONY=${WORKSPACE_INSTALL_SYMFONY}
          - INSTALL_PYTHON=${WORKSPACE_INSTALL_PYTHON}
          - INSTALL_IMAGE_OPTIMIZERS=${WORKSPACE_INSTALL_IMAGE_OPTIMIZERS}
          - INSTALL_IMAGEMAGICK=${WORKSPACE_INSTALL_IMAGEMAGICK}
          - PUID=${WORKSPACE_PUID}
          - PGID=${WORKSPACE_PGID}
          - NODE_VERSION=${WORKSPACE_NODE_VERSION}
          - YARN_VERSION=${WORKSPACE_YARN_VERSION}
          - TZ=${WORKSPACE_TIMEZONE}
          - BLACKFIRE_CLIENT_ID=${BLACKFIRE_CLIENT_ID}
          - BLACKFIRE_CLIENT_TOKEN=${BLACKFIRE_CLIENT_TOKEN}
        dockerfile: "Dockerfile-${PHP_VERSION}"
      volumes_from:
        - applications
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      ports:
        - "${WORKSPACE_SSH_PORT}:22"
      tty: true
      networks:
        - frontend
        - backend

### PHP-FPM Container #######################################

    php-fpm:
      build:
        context: ./php-fpm
        args:
          - INSTALL_XDEBUG=${PHP_FPM_INSTALL_XDEBUG}
          - INSTALL_BLACKFIRE=${INSTALL_BLACKFIRE}
          - INSTALL_SOAP=${PHP_FPM_INSTALL_SOAP}
          - INSTALL_MONGO=${PHP_FPM_INSTALL_MONGO}
          - INSTALL_MSSQL=${PHP_FPM_INSTALL_MSSQL}
          - INSTALL_ZIP_ARCHIVE=${PHP_FPM_INSTALL_ZIP_ARCHIVE}
          - INSTALL_BCMATH=${PHP_FPM_INSTALL_BCMATH}
          - INSTALL_PHPREDIS=${PHP_FPM_INSTALL_PHPREDIS}
          - INSTALL_MEMCACHED=${PHP_FPM_INSTALL_MEMCACHED}
          - INSTALL_OPCACHE=${PHP_FPM_INSTALL_OPCACHE}
          - INSTALL_EXIF=${PHP_FPM_INSTALL_EXIF}
          - INSTALL_AEROSPIKE=${PHP_FPM_INSTALL_AEROSPIKE}
          - INSTALL_MYSQLI=${PHP_FPM_INSTALL_MYSQLI}
          - INSTALL_TOKENIZER=${PHP_FPM_INSTALL_TOKENIZER}
          - INSTALL_INTL=${PHP_FPM_INSTALL_INTL}
          - INSTALL_GHOSTSCRIPT=${PHP_FPM_INSTALL_GHOSTSCRIPT}
          - INSTALL_LDAP=${PHP_FPM_INSTALL_LDAP}
          - INSTALL_SWOOLE=${PHP_FPM_INSTALL_SWOOLE}
          - INSTALL_IMAGE_OPTIMIZERS=${PHP_FPM_INSTALL_IMAGE_OPTIMIZERS}
          - INSTALL_IMAGEMAGICK=${PHP_FPM_INSTALL_IMAGEMAGICK}
        dockerfile: "Dockerfile-${PHP_VERSION}"
      volumes_from:
        - applications
      volumes:
        - ./php-fpm/php${PHP_VERSION}.ini:/usr/local/etc/php/php.ini
      expose:
        - "9000"
      depends_on:
        - workspace
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      environment:
        - PHP_IDE_CONFIG=${PHP_IDE_CONFIG}
      networks:
        - backend

### PHP Worker Container #####################################

    php-worker:
      build:
        context: ./php-worker
        dockerfile: "Dockerfile-${PHP_VERSION}"
      volumes_from:
        - applications
      depends_on:
        - workspace
      extra_hosts:
        - "dockerhost:${DOCKER_HOST_IP}"
      networks:
        - backend

### NGINX Server Container ##################################

    nginx:
      build:
        context: ./nginx
        args:
          - PHP_UPSTREAM_CONTAINER=${NGINX_PHP_UPSTREAM_CONTAINER}
          - PHP_UPSTREAM_PORT=${NGINX_PHP_UPSTREAM_PORT}
      volumes_from:
        - applications
      volumes:
        - ${NGINX_HOST_LOG_PATH}:/var/log/nginx
        - ${NGINX_SITES_PATH}:/etc/nginx/sites-available
      ports:
        - "${NGINX_HOST_HTTP_PORT}:80"
        - "${NGINX_HOST_HTTPS_PORT}:443"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### Minio Container #########################################

    minio:
      build: ./minio
      volumes:
        - minio:/export
      ports:
        - "${MINIO_PORT}:9000"
      environment:
        - MINIO_ACCESS_KEY=access
        - MINIO_SECRET_KEY=secretkey
      networks:
        - frontend

### MongoDB Container #######################################

    mongo:
      build: ./mongo
      ports:
        - "${MONGODB_PORT}:27017"
      volumes:
        - ${DATA_SAVE_PATH}/mongo:/data/db
      networks:
        - backend

### Redis Container #########################################

    redis:
      build: ./redis
      volumes:
        - ${DATA_SAVE_PATH}/redis:/data
      ports:
        - "${REDIS_PORT}:6379"
      networks:
        - backend

### Memcached Container #####################################

    memcached:
      build: ./memcached
      volumes:
        - ${DATA_SAVE_PATH}/memcached:/var/lib/memcached
      ports:
        - "${MEMCACHED_HOST_PORT}:11211"
      depends_on:
        - php-fpm
      networks:
        - backend

### Caddy Server Container ##################################

    caddy:
      build: ./caddy
      volumes_from:
        - applications
      volumes:
        - ${CADDY_CUSTOM_CADDYFILE}:/etc/Caddyfile
        - ${CADDY_HOST_LOG_PATH}:/var/log/caddy
        - ${DATA_SAVE_PATH}:/root/.caddy
      ports:
        - "${CADDY_HOST_HTTP_PORT}:80"
        - "${CADDY_HOST_HTTPS_PORT}:443"
      depends_on:
        - php-fpm
      networks:
        - frontend
        - backend

### ElasticSearch Container #################################

#    elasticsearch:
#      build: ./elasticsearch
#      volumes:
#        - elasticsearch-data:/usr/share/elasticsearch/data
#        - elasticsearch-plugins:/usr/share/elasticsearch/plugins
#      environment:
#        - cluster.name=laradock-cluster
#        - bootstrap.memory_lock=true
#        - "ES_JAVA_OPTS=-Xms256m -Xmx256m"
#      ulimits:
#        memlock:
#          soft: -1
#          hard: -1
#      mem_limit: 512m
#      ports:
#        - "${ELASTICSEARCH_HOST_HTTP_PORT}:9200"
#        - "${ELASTICSEARCH_HOST_TRANSPORT_PORT}:9300"
#      depends_on:
#        - php-fpm
#      networks:
#        - frontend
#        - backend

### Certbot Container ##################################

    certbot:
      build:
        context: ./certbot
      volumes:
        - ./data/certbot/certs/:/var/certs
        - ./certbot/letsencrypt/:/var/www/letsencrypt
      environment:
        - CN="fake.domain.com"
        - EMAIL="fake.email@gmail.com"
      networks:
        - frontend

### Mailhog Container #########################################

    mailhog:
      build: ./mailhog
      ports:
        - "1025:1025"
        - "8025:8025"
      networks:
        - frontend
        - backend

### Selenium Container ########################################

    selenium:
      build: ./selenium
      ports:
        - "${SELENIUM_PORT}:4444"
      volumes:
        - /dev/shm:/dev/shm
      networks:
        - frontend

### Networks Setup ############################################

networks:
  frontend:
    driver: "bridge"
  backend:
    driver: "bridge"

### Volumes Setup #############################################

volumes:
  redis:
    driver: "local"
  mongo:
    driver: "local"
  minio:
    driver: "local"
  caddy:
    driver: "local"