@extends('_layouts.master')

@section('body')
    <div class="wrapper">
        <div class="error-page">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h1 class="h1 font-weight-bold text-uppercase">Whoops!</h1>
                        <h2 class="h3 font-weight-light">
                            {!! __('The Sharing Network you\'re looking for <br> is not here or it never existed!') !!}
                        </h2>
                        <br>
                        @auth
                            @if (auth()->user()->sharingNetworks()->count() === 0)
                                <a href="{{ route('share.create') }}" class="btn btn-outline-primary text-uppercase btn-lg">
                                    {{ __('Start your First Sharing Network') }}
                                </a>
                            @else
                                <a href="{{ route('account.memberships') }}" class="btn btn-outline-primary btn-lg">
                                    {{ __('Access your memberships') }}
                                </a>
                            @endif
                            @else
                                <a href="{{ route('auth.register') }}" class="btn btn-outline-primary text-uppercase btn-lg">
                                    {{ __('Start now') }}
                                </a>
                                @endauth
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection