@extends('_layouts.master')

@section('body')
    <div class="wrapper">
        <div class="error-page">
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        <h1 class="h1 font-weight-bold text-uppercase">503!</h1>
                        <h2 class="h3 font-weight-light">
                            {{ __('Something is wrong! (But our tech guys are getting to it)') }}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection