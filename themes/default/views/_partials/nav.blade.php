<nav class="navbar navbar-expand-lg navbar-light bg-default">
    <div class="container">
        <a class="navbar-brand" href="@auth {{ route('account.memberships')  }} @else {{ route('site.home') }} @endauth">
            <img src="{{ theme('img/anyshare-logo-squares.png') }}" width="200px" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbar1">
            <ul class="navbar-nav">
                @guest
                    <li class="nav-item">
                        <a href="{{ route('auth.login') }}" class="nav-link">{{ __('Sign in') }}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('auth.register') }}" class="nav-link">{{ __('Start') }}</a>
                    </li>
                    @else
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ auth()->user()->profile->name }}
                            </a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">{{ __('My Profile') }}</a>
                                <a class="dropdown-item" href="{{ route('account.memberships') }}">{{ __('Sharing Networks') }}</a>
                                <a class="dropdown-item" href="#">{{ __('Orders') }}</a>
                                <a class="dropdown-item" href="{{ route('auth.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        <li class="nav-item">
                            @if (auth()->user()->sharingNetworks()->count() === 0)
                                <a href="{{ route('share.create') }}" class="btn btn-primary text-uppercase">
                                    {{ __('Start your First Sharing Network') }}
                                </a>
                            @else
                                <a href="{{ route('share.create') }}" class="btn btn-outline-primary">
                                    {{ __('New Sharing Network') }}
                                </a>
                            @endif
                        </li>
                        @endguest
            </ul>
        </div>
    </div>
</nav>