<nav class="navbar navbar-expand-lg navbar-light bg-default">
    <div class="container">
        <a class="navbar-brand col-6" href="@auth {{ route('account.memberships')  }} @else {{ route('site.home') }} @endauth">
            <span class="h2 font-weight-light">
                <span class="mr-4">{{ $tenant->name }}</span>
            </span>
            <img src="{{ theme('img/anyshare-logo-squares.png') }}" height="20px" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="col-6 collapse navbar-collapse justify-content-end" id="navbar1">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="{{ tenant_route('tenant.home') }}" class="nav-link">{{ __('Browse') }}</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">{{ __('Members') }}</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">{{ __('About') }}</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ auth()->user()->profile->name }}
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">{{ __('My Profile') }}</a>
                        <a class="dropdown-item" href="{{ route('account.memberships') }}">{{ __('Sharing Networks') }}</a>
                        <a class="dropdown-item" href="#">{{ __('Orders') }}</a>
                        <a class="dropdown-item" href="{{ route('auth.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('auth.logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>