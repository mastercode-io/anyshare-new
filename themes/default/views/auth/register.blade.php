@extends('_layouts.master')

@section('body')
    <div class="container">
        <div class="row justify-content-md-center mt-5">
            <div class="col-sm-12 col-md-5">
                @component('components.card', ['title' => __('Input your information to sign up')])
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach

                    {{ html()->form('POST', route('auth.register'))->open() }}

                    @component('components.form-group', ['label' => 'Name', 'field' => 'name'])
                        {{ html()->text('name')->class('form-control')->placeholder(__('Name')) }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'E-mail', 'field' => 'email'])
                        {{ html()->email('email')->class('form-control')->placeholder(__('E-mail')) }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'Password', 'field' => 'password'])
                        {{ html()->password('password')->class('form-control')->placeholder(__('Password')) }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'Confirm password', 'field' => 'password_confirmation'])
                        {{ html()->password('password_confirmation')->class('form-control')->placeholder(__('Confirm password')) }}
                    @endcomponent

                    @component('components.form-group', ['field' => 'terms_accepted'])
                        @component('components.checkbox', ['field' => 'terms_accepted'])
                            {{ __('I accept the') }} <a href="#">{{ __('Terms and Conditions') }}</a>
                        @endcomponent
                    @endcomponent

                    <div class="text-center">
                        <button type="submit" class="btn btn-filled btn-log margin-right">
                            <i class="fa fa-sign-in"></i> <span>{{ __('Sign in') }}</span>
                        </button>
                    </div>
                @endcomponent
            </div>
        </div>
    </div>
@endsection
