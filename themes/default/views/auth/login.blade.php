@extends('_layouts.master')

@section('body')

    <div class="container">
        <div class="row justify-content-md-center mt-5">
            <div class="col-sm-12 col-md-5">
                @component('components.card', ['title' => __('Input your credentials to sign in')])
                    {{ html()->form('POST', route('auth.login'))->open() }}

                    @component('components.form-group', ['label' => 'E-mail', 'field' => 'email'])
                        {{ html()->email('email')->class('form-control')->placeholder(__('E-mail'))->required()->autofocus() }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'Password', 'field' => 'password'])
                        {{ html()->password('password')->class('form-control')->placeholder(__('Password'))->required()->autofocus() }}
                    @endcomponent

                    @component('components.form-group', ['field' => 'remember'])
                        @component('components.checkbox', ['field' => 'remember'])
                            {{ __('Remember me') }}
                        @endcomponent
                    @endcomponent

                    <div class="text-center">
                        <button type="submit" class="btn btn-filled btn-lg btn-primary margin-right">
                            <i class="fa fa-sign-in"></i> <span>{{ __('Sign in') }}</span>
                        </button>
                    </div>

                    <div class="text-center">
                        <a class="btn btn-link" href="{{ route('auth.password.request') }}">
                            {{ __('Forget the password?') }}
                        </a>
                    </div>
            </div>
            {{ html()->form()->close() }}
            @endcomponent
        </div>
    </div>
    </div>
@endsection
