@extends('_layouts.master')

@section('body')

    <div class="wrapper">
        <!-- Content Area Start -->
        <div id="content">

            <!-- Header with Search form -->
            <div class="jumbotron hero hero--home">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1 class="hero__title">{{ __('The New Way To Share') }}</h1>
                            <h3 class="h5 mt-4 mb-5 hero__tagline">{{ __('AnyShare makes it easy to share skills, things and ideas within any group or community.') }}</h3>
                            <a href="{{ route('auth.register') }}" class="btn btn-primary btn-lg text-uppercase">
                                {{ __('Start') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

<!doctype html>