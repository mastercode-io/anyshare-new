@extends('_layouts.master')

@section('body')

    <div class="wrapper">
        <!-- Content Area Start -->
        <div id="content">

            <!-- Header with Search form -->
            <div class="jumbotron hero hero--section">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1 class="hero__title">{{ __('Edit :name Sharing Network') }}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                {{ html()->modelForm($share, 'PUT', route('share.update', $share->getKey()))->class('row')->open() }}
                <div class="col-sm-12 col-md">
                    @component('components.form-group', ['label' => 'Name *', 'field' => 'name'])
                        {{ html()->text('name')->class('form-control')->placeholder(__('Sharing Network Name'))->required() }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'URL (***.anyshare.coop) *', 'field' => 'subdomain'])
                        {{ html()->text('subdomain')->class('form-control')->required() }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'Privacy level', 'field' => 'name'])
                        {{ html()->select('privacy_level', $privacy_levels->toArray(), '')->class('form-control') }}
                    @endcomponent
                </div>
                <div class="col-sm-12 col-md text-right text-uppercase">
                    <button class="btn btn-primary">
                        {{ __('Save') }}
                    </button>
                </div>
                {{ html()->form()->closeModelForm() }}
            </div>

        </div>
    </div>
@endsection

<!doctype html>