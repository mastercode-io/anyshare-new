@extends('_layouts.master')

@section('body')

    <div class="wrapper">
        <!-- Content Area Start -->
        <div id="content">

            <!-- Header with Search form -->
            <div class="jumbotron hero hero--section">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1 class="hero__title">{{ __('Start a Sharing Network') }}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                {{ html()->form('POST', route('share.store'))->class('row')->open() }}
                <div class="col-sm-12 col-md">
                    @component('components.form-group', ['label' => 'Choose a name for your Sharing Network *', 'field' => 'name'])
                        {{ html()->text('name')->class('form-control')->placeholder(__('Sharing Network Name'))->required() }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'Choose a URL (***.' . config('app.domain') . ') for your Sharing Network *', 'field' => 'subdomain'])
                        {{ html()->text('subdomain')->class('form-control')->required() }}
                    @endcomponent

                    @component('components.form-group', ['label' => 'Choose a privacy level', 'field' => 'name'])
                        {{ html()->select('privacy_level', $privacy_levels->toArray(), '')->class('form-control') }}
                    @endcomponent
                </div>
                <div class="col-sm-12 col-md text-right text-uppercase">
                    <button class="btn btn-primary">
                        {{ __('Start your 30 Day Free Trial') }}
                    </button>
                </div>
                {{ html()->form()->close() }}
            </div>

        </div>
    </div>
@endsection

<!doctype html>