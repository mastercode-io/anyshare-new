@extends('_layouts.master')

@section('body')

    <div class="wrapper">
        <!-- Content Area Start -->
        <div id="content">

            <!-- Header with Search form -->
            <div class="jumbotron hero hero--section">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1 class="hero__title">{{ __('Memberships') }}</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col">
                        <table class="table mt-5">
                            <thead>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Where') }}</th>
                            <th width="120">{{ __('Action') }}</th>
                            </thead>
                            <tbody>
                            @foreach ($sharing_networks as $share)
                                <tr>
                                    <td>
                                        <a href="{{ $share->url }}">{{ $share->name }}</a> <br>
                                        <small class="text-muted">{{ $share->privacy_level_label }} {{ __('Community') }}</small>
                                    </td>
                                    <td></td>
                                    <td>
                                        <a href="{{ $share->url }}" class="btn btn-outline-primary">{{ __('Enter') }}</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

<!doctype html>