@extends('_layouts.master')

@section('body')

    <div class="wrapper">
        <!-- Content Area Start -->
        <div id="content">

            <!-- Header with Search form -->
            <div class="jumbotron hero hero--section">
                <div class="container">
                    <div class="row">
                        <div class="col text-center">
                            <h1 class="hero__title">{{ $tenant->name }}</h1>
                            <h2 class="hero__tagline">{{ __('Sharing Network') }}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col">
                        <table class="table mt-5">
                            <thead>
                            <th>{{ __('Image') }}</th>
                            <th>{{ __('Type') }}</th>
                            <th>{{ __('What') }}</th>
                            <th>{{ __('Posted by') }}</th>
                            <th>{{ __('How') }}</th>
                            <th>{{ __('Where') }}</th>
                            <th>{{ __('Created at') }}</th>
                            </thead>
                            <tbody>
                            @forelse ($entries as $entry)
                                <tr>
                                    <td> -</td>
                                    <td>{{ $entry->type_label }}</td>
                                    <td><a href="{{ $entry->url }}">{{ $entry->title }}</a></td>
                                    <td>{{ $entry->user->name }}</td>
                                    <td>{{ $entry->exchange_types_label }}</td>
                                    <td> -</td>
                                    <td>{{ $entry->created_at }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="7" class="text-center">{{ __('No entry was added yet.') }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

<!doctype html>