<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="{{ config('app.description', 'Auto Fácil Joinville') }}">
    <meta name="keywords" content="auto, facil, joinville, carros, automóveis, classificados, veículos, semi novos, usados, nacionais, importados, revendas, concessionárias">
    <meta name="author" content="MasterCode">

    <title>{{ config('app.name') }}</title>

    <!-- CSS Files -->
    <link rel="stylesheet" href="{{  theme('css/app.css') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ theme('img/favicon.png') }}">

    <!-- Fonts icons -->
    <link rel="stylesheet" href="{{ theme('css/font-awesome.min.css') }}">

</head>

<body class="bg-white">

<div class="wrapper">
    <!-- Content Area Start -->
    <div id="content">
        @if (isset(request()->tenant))
            @component('_partials.nav-tenant') @endcomponent
        @else
            @component('_partials.nav') @endcomponent
        @endif

        <div class="mb-60"></div>

        @yield('body')
    </div>
</div>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="{{ theme('js/app.js') }}"></script>
</body>

</html>