{{--
    Render a form group.

    @param $field   Field identifier
    @param $label   Label of the field
--}}
<div class="form-group {{ $errors->has($field) ? 'is-invalid' : '' }}">
    @if (isset($label))
        {{ html()->label(__($label), $field) }}
    @endif

    {{ $slot }}

    @if ($errors->has($field))
        <div class="invalid-feedback">
            <small>{{ $errors->first($field) }}</small>
        </div>
    @endif
</div>