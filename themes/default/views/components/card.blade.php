<div class="card">
    @if (isset($title))
        <div class="card-header text-center">
            {{ $title }}
        </div>
    @endif
    <div class="card-body">
        {{ $slot }}
    </div>
</div>