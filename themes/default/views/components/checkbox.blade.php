@php
    $checkboxId = $id ?? $field . '-' . str_random(8);
@endphp

<div class="custom-control custom-checkbox">
    {{ html()->checkbox($field, $checked ?? false, $value ?? '1')->class('custom-control-input')->id($checkboxId) }}
    <label class="custom-control-label" for="{{ $checkboxId }}">
        {{ $slot }}
    </label>
</div>