module.exports = [
    { type: 'js', src: 'assets/js/app.js', dest: 'js' },
    { type: 'sass', src: 'assets/sass/app.scss', dest: 'css' },
    { type: 'copy', src: 'assets/css', dest: 'css' },
    { type: 'copy', src: 'assets/fonts', dest: 'fonts' },
    { type: 'copy', src: 'assets/img', dest: 'img' },
];