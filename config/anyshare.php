<?php

return [
    'name' => 'Core',

    'sharing-networks' => [
        'privacy-levels' => [
            'O' => 'Opened',
            'C' => 'Closed',
        ],
    ],
];
