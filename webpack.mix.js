let mix = require('laravel-mix');
const { lstatSync, readdirSync, readFileSync } = require('fs');
const { normalize, join, resolve } = require('path');

function isDirectory (source) {
    return lstatSync(resolve(__dirname, 'themes', source)).isDirectory()
}

function getThemes () {
    return readdirSync(__dirname + '/themes')
        .filter(isDirectory)
        .map(function (name) {
            var themeMix = require(join(__dirname, '/themes', name, '/mix.js'))

            return {
                name: name,
                mix: themeMix
            };
        });
}

function destPath (theme, path) {
    return join('public', 'modules', theme.name, path);
}

function srcPath (theme, path) {
    return join('themes', theme.name, path);
}

/*
 * Compile all themes.
 */

mix.options({
    processCssUrls: false,
});

getThemes().forEach(function (theme) {
    theme.mix.forEach(function (file) {
        mix[file.type](srcPath(theme, file.src), destPath(theme, file.dest));
    });
});