<?php

/*
 * Tenant routes.
 */
Route::domain('{tenant}.' . config('app.domain'))->as('tenant.')->middleware(['auth', \App\Http\Middleware\TenantMiddleware::class])->group(function () {
    Route::get('/', Tenant\HomeHandler::class)->name('home');

    Route::get('/share/edit', SharingNetworks\EditSharingNetworkHandler::class)->name('share.edit');
    Route::put('/share/edit', SharingNetworks\UpdateSharingNetworkHandler::class)->name('share.update');
});

/*
 * Authentication and registering routes
 */
Route::domain(config('app.domain'))->group(function () {
    Route::get('login', 'Auth\\LoginController@showLoginForm')->name('auth.login')->middleware('guest');
    Route::post('login', 'Auth\\LoginController@login')->middleware('guest');
    Route::post('logout', 'Auth\\LoginController@logout')->name('auth.logout')->middleware('auth');

    Route::post('password/email', 'Auth\\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.email')->middleware('guest');
    Route::get('password/reset', 'Auth\\ForgotPasswordController@showLinkRequestForm')->name('auth.password.request')->middleware('guest');
    Route::post('password/reset', 'Auth\\ResetPasswordController@reset')->middleware('guest');
    Route::get('password/reset/{token}', 'Auth\\ResetPasswordController@showResetForm')->name('auth.password.reset')->middleware('guest');

    Route::get('register', 'Auth\\RegisterController@showRegistrationForm')->name('auth.register')->middleware('guest');
    Route::post('register', 'Auth\\RegisterController@register')->middleware('guest');
});

/*
 * Website routes.
 */
Route::domain(config('app.domain'))->group(function () {
    Route::get('/', Site\HomeHandler::class)->name('site.home')->middleware('guest');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/account/memberships', Account\ListMembershipsHandler::class)->name('account.memberships');

        Route::get('/share/new', SharingNetworks\CreateSharingNetworkHandler::class)->name('share.create');
        Route::post('/share/new', SharingNetworks\StoreSharingNetworkHandler::class)->name('share.store');
    });
});
